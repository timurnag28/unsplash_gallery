import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
class PhotoPage extends StatelessWidget {
  final String photoUrl;
  final bool error;
  PhotoPage({Key key, this.photoUrl, this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return !error
        ? Container(
          color: Colors.black,
          child: Image.network(
            photoUrl,
            fit: BoxFit.contain,
          ),
        )
        : Container(
            child: Center(
                child: Text(
              'Ошибка получения данных',
              style: TextStyle(color: Colors.white, fontSize: 18),
            )),
          );
  }
}
