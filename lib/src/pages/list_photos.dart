import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:unsplash_gallery/src/pages/photo.dart';
import 'package:unsplash_gallery/src/stores/global_store.dart';
import 'package:unsplash_gallery/src/stores/unsplash_data_store.dart';

class ListPhotosPage extends StatefulWidget {
  @override
  ListPhotosPageState createState() => ListPhotosPageState();
}

class ListPhotosPageState extends State<ListPhotosPage> {
  UnSplashStore unSplashStore = GlobalStore().unSplashStore;
  var subscription;

  @override
  void initState() {
    super.initState();
    unSplashStore.getData();

    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile) {
        if (unSplashStore.isError) {
          unSplashStore.getData();
        }
      } else if (connectivityResult == ConnectivityResult.wifi) {
        if (unSplashStore.isError) {
          unSplashStore.getData();
        }
      } else {
        unSplashStore.connectionClose();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    subscription.cancel();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: Observer(builder: (context) {
        if (unSplashStore.isLoading)
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        if (unSplashStore.isError)
          return Container(
            child: Center(
              child: Text('Ошибка получения данных'),
            ),
          );
        return ListView.builder(
          itemCount: unSplashStore.data == null ? 0 : unSplashStore.data.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => PhotoPage(
                                    photoUrl:
                                        unSplashStore.data[index].urls.small,
                                    error: unSplashStore.isError,
                                  )));
                    },
                    leading: Image.network(
                      unSplashStore.data[index].urls.thumb,
                      width: 100,
                    ),
                    title: Text(
                      unSplashStore.data[index].user.name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                        unSplashStore.data[index].altDescription != null
                            ? unSplashStore.data[index].altDescription
                            : 'Название отсутствует.'),
                    isThreeLine: true,
                  ),
                ),
              ],
            );
          },
        );
      })),
    );
  }
}
