import 'package:mobx/mobx.dart';
import 'package:unsplash_gallery/src/api/api.dart';
import 'package:unsplash_gallery/src/models/unsplash.dart';

part 'unsplash_data_store.g.dart';

class UnSplashStore = UnSplashDataStore with _$UnSplashStore;

abstract class UnSplashDataStore with Store {
  @observable
  List<UnSplash> data;

  @observable
  bool isError = false;

  @observable
  bool isLoading = false;

  @action
  getData() async {
    isLoading = true;
    isError = false;
    await API().unSplashAPI.getPhotosData().then((photos) {
      data = photos;
      print('success');
    }).catchError((e) {
      print('error');
      isError = true;
    });
    isLoading = false;
  }

  @action
  connectionClose() async {
    isError = true;
    print('connection close');
  }
}
