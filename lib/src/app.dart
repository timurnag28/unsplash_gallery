import 'package:flutter/material.dart';
import 'package:unsplash_gallery/src/pages/list_photos.dart';

class App extends StatefulWidget {
  @override
  State createState() => new MyWidgetState();
}

class MyWidgetState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: ListPhotosPage());
  }
}
