import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:unsplash_gallery/src/models/unsplash.dart';

final token = 'it1PXzVQRnxgz8v8hazcst7G9rNfXk1qiS8FgHTTMMk';

class UnSplashAPI {
  Future<List<UnSplash>> getPhotosData() async {
    try {
      final response = await http
          .get('https://api.unsplash.com/photos?per_page=30&client_id=$token');
      print('-------------GET_UNSPLASH_PHOTOS------------');
      return unSplashFromJson(jsonEncode(jsonDecode(response.body)));
    } catch (err) {
      throw err;
    }
  }
}
